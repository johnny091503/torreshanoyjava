/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package torreshanoy;

import java.util.Scanner;

/**
 *
 * @author JHONNY
 */
public class TorresHanoy {

    /**
     * @param args the command line arguments
     * 
     * 
     */
    
    public void torresH(int NumDiscos,int torre1,int torre2,int torre3){
        
          
          
        if(NumDiscos == 1)
        {
            System.out.println("Mover de la torre: "+torre1+" a la torre : "+torre3);
        }
       
       
        
           else
        {
            torresH(NumDiscos-1, torre1, torre3, torre2);
            
            System.out.println("Mover de la torre: "+torre1+" a la torre : "+torre3);
            
            torresH(NumDiscos-1, torre2, torre1, torre3);
            
              
        }
        
       
    }
    
    public static void main(String[] args) {
        // TODO code application logic here
        int n=0;
        Scanner leer = new Scanner(System.in);
        TorresHanoy hanoy = new TorresHanoy();
        
        System.out.print("Numero de discos: ");
        n = leer.nextInt();
        
        
        
        hanoy.torresH(n, 1, 2, 3);
    }
    
}
